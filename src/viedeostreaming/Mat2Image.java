package viedeostreaming;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;
public class Mat2Image {
	 Mat matrix;
	    MatOfByte mob;
	    String fileExten;
	public Mat2Image(){

	}

	public Mat2Image(Mat amatrix, String fileExt){
	    matrix = amatrix;
	    fileExten = fileExt;
	}
	public void setMatrix(Mat amatrix, String fileExt){
	    matrix = amatrix;
	    fileExten = fileExt;
	    mob = new MatOfByte();
	}
	public BufferedImage getBufferedImage(){
		Imgcodecs.imencode(fileExten, matrix, mob);
	    byte[] byteArray = mob.toArray();
	    BufferedImage bufImage = null;
	    try{
	        InputStream in = new ByteArrayInputStream(byteArray);
	        bufImage = ImageIO.read(in);
	    }catch(Exception e){
	        e.printStackTrace();
	    }
	    return bufImage;
	    }
//    static{
//        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//    }
//
//    Mat mat = new Mat();
//    BufferedImage img;
//
//    public Mat2Image() {
//    }
//
//    public Mat2Image(Mat mat) {
//        getSpace(mat);
//    }
//
//    public void getSpace(Mat mat) {
//        int type = 0;
//        if (mat.channels() == 1) {
//            type = BufferedImage.TYPE_BYTE_GRAY;
//        } else if (mat.channels() == 3) {
//            type = BufferedImage.TYPE_3BYTE_BGR;
//        }
//        this.mat = mat;
//        int w = mat.cols();
//        int h = mat.rows();
//        if (img == null || img.getWidth() != w || img.getHeight() != h || img.getType() != type)
//            img = new BufferedImage(w, h, type);
//    }
//
//    BufferedImage getImage(Mat mat){
//        getSpace(mat);
//        WritableRaster raster = img.getRaster();
//        DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
//        byte[] data = dataBuffer.getData();
//        mat.get(0, 0, data);
//        return img;
//    }
}
