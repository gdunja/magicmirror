package viedeostreaming;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

import testOpen.Recognition;
import view.Window;

public class Facedetect {

	static JFrame frame;
	static JLabel lbl;
	static ImageIcon icon;

	public static void main(String[] args) {
		Facedetect facedetect = new Facedetect();
		Window window = new Window();
		facedetect.startFaceDetection(window);
	}

	public void startFaceDetection(Window window) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		window.showEmptyWindow();
		// System.out.println("l�dt window");
		CascadeClassifier cascadeFaceClassifier = new CascadeClassifier(
				"..\\FirstOpenCV\\src\\viedeostreaming\\haarcascade_frontalface_alt.xml");
		VideoCapture videoDevice = new VideoCapture(0);
		
		if (videoDevice.isOpened()) {
			while (true) {
				Mat frameCapture = new Mat();
				videoDevice.read(frameCapture);

				MatOfRect faces = new MatOfRect();
				cascadeFaceClassifier.detectMultiScale(frameCapture, faces);
				Rect rectCrop = null;
				if (!faces.empty()) {
					for (Rect rect : faces.toArray()) {
						Imgproc.putText(frameCapture, "Face", new Point(rect.x, rect.y - 5), 1, 2,
								new Scalar(0, 0, 255));
						Imgproc.rectangle(frameCapture, new Point(rect.x, rect.y),
								new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 100, 0), 3);
						rectCrop = new Rect(rect.x, rect.y, rect.width, rect.height);

					}
					// System.out.println(String.format("%s (FACES) detected.",
					// faces.toArray().length));
					Mat image_roi = new Mat(frameCapture, rectCrop);
					Mat img = new Mat();
					Imgproc.resize(image_roi, img, new Size(100, 100));
					Imgcodecs.imwrite("../FirstOpenCV/NeuerOrdner/out.jpg", img);
					Imgcodecs.imwrite("../FirstOpenCV/NeuerOrdner/wholeOut.jpg", frameCapture);
					break;

					// pushImage(convertMat2Image(frameCapture));

				}

			}
			videoDevice.release();
			videoDevice = null;
			
			System.gc();
			
			Recognition rec = new Recognition();
			rec.start(window);
		} else {
			System.out.println("Kein Video/ Kamera gefunden.");
			return;
		}
	}

	// private static BufferedImage convertMat2Image(Mat kameraVerisi) {
	//
	// MatOfByte byteMatVerisi = new MatOfByte();
	// Imgcodecs.imencode(".jpg", kameraVerisi, byteMatVerisi);
	// byte[] byteArray = byteMatVerisi.toArray();
	// BufferedImage goruntu = null;
	// try {
	// InputStream in = new ByteArrayInputStream(byteArray);
	// goruntu = ImageIO.read(in);
	// } catch (Exception e) {
	// e.printStackTrace();
	// return null;
	// }
	// return goruntu;
	// }

	// public static void fenster() {
	// frame = new JFrame();
	// frame.setLayout(new FlowLayout());
	// frame.setSize(700, 600);
	// frame.setVisible(true);
	// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	// }

	// public static void pushImage(BufferedImage img2) {
	// if (frame == null)
	// fenster();
	// if (lbl != null)
	// frame.remove(lbl);
	// icon = new ImageIcon(img2);
	// lbl = new JLabel();
	// lbl.setIcon(icon);
	// frame.add(lbl);
	// frame.revalidate();
	//
	//// try (OutputStream stream = new
	// FileOutputStream("C:\\Users\\Dunja\\Documents\\DHBW\\Seminararbeit\\testout.jpg"))
	// {
	//// ImageIO.write(img2, "jpg", stream);
	//// } catch (IOException e) {
	//// e.printStackTrace();
	//// }
	//
	// }
}
