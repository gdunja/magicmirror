package viedeostreaming;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.bytedeco.javacpp.opencv_text.OCRBeamSearchDecoder.ClassifierCallback;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

public class MyFrame extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	// ***********************************************************************************************
	private BufferedImage image;
	int count = 1;

	// ***********************************************************************************************
	public MyFrame() {
		super();
	}

	// ***********************************************************************************************
	public BufferedImage getimage() {
		return image;
	}

	// ***********************************************************************************************
	public void setimage(BufferedImage newimage) {
		image = newimage;
		return;
	}

	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		if (this.image == null)
			return;
		g.drawImage(this.image, 10, 492, 650, 43, this.image.getWidth(), this.image.getHeight(), count, count, null);
	}

	public void DatainIt() throws Exception {
		JFrame frame = new JFrame("Face Detection");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String xmlFile = "C:\\Users\\Dunja\\workspaceub\\FirstOpenCV\\src\\viedeostreaming\\haarcascade_frontalface_alt.xml";
		CascadeClassifier classifier = new CascadeClassifier(xmlFile);
		MyFrame myframe = new MyFrame();
		frame.setContentPane(myframe);

		// **************************************
		// BUTTON
		JButton save = new JButton("Add a new Person");
		// save.setBounds(2, 2, 30, 80);
		JPanel pbutton = new JPanel();
		pbutton.add(save);
		// TextField
		JTextField p_name = new JTextField(25);
		frame.add(p_name);
		frame.add(pbutton);
		frame.setVisible(true);
		save.addActionListener(this);
		// **************************************

		Mat webcam_image = new Mat();
		Mat2Image mat2Buf = new Mat2Image();

		VideoCapture capture = new VideoCapture();
		if (capture.isOpened()) {
			Thread.sleep(100);
			while (true) {
				capture.read(webcam_image);
				if (!webcam_image.empty()) {
                    frame.setSize(webcam_image.width()+40, webcam_image.height()+60);
					MatOfRect faceDetections = new MatOfRect();
					classifier.detectMultiScale(webcam_image, faceDetections);
					for (Rect rect : faceDetections.toArray()) {
						//imgproc statt core?!?!?
						Imgproc.rectangle(webcam_image, new Point(rect.x, rect.y),
								new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 0, 255), 3);
					}
					mat2Buf.setMatrix(webcam_image, ".jpg");
					myframe.setimage(mat2Buf.getBufferedImage());
					myframe.repaint();
					// Mat image_roi = new Mat(webcam_image, rect);
					// Mat img = new Mat();
					// Imgcodecs.imwrite("C:\\Users\\Dunja\\workspaceub\\FirstOpenCV\\facedetect_facetest.jpg",
					// img);

				} else {
					System.out.println("Problems with WebCam Capture");
					break;
				}

			}
		}
		capture.release();
	}
	   public static void main(String arg[]) throws Exception{
	        MyFrame fra = new MyFrame();
	        fra.DatainIt();
	    }//end main
	    //***********************************************************************************************   
	    public void actionPerformed(ActionEvent arg0) {

	    }
	// private JPanel contentPane;
	//
	// public static void main(String[] args) {
	//
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// MyFrame frame = new MyFrame();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	// public MyFrame() {
	//
	// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	// setBounds(0, 0, 1280, 720);
	// contentPane = new JPanel();
	// contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
	// setContentPane(contentPane);
	// contentPane.setLayout(null);
	//
	// new MyThread().start();
	// }
	//
	//
	// public void paint(Graphics g){
	// // VideoCap videoCap = new VideoCap();
	//
	//
	//
	//
	//
	// g = contentPane.getGraphics();
	// g.drawImage(webcam_image, 0, 0, this);
	// }

	// class MyThread extends Thread{
	// @Override
	// public void run() {
	// for (;;){
	// repaint();
	// try { Thread.sleep(30);
	// } catch (InterruptedException e) { }
	// }
	// }
	// }
}
