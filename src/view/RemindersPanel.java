package view;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.api.services.calendar.model.Event;

public class RemindersPanel extends JPanel {
	private JLabel remindersLabel;

	public RemindersPanel() {
		remindersLabel = new JLabel();
		remindersLabel.setForeground(Color.WHITE);
		remindersLabel.setFont(new Font("Arial", Font.BOLD, 15));
		setOpaque(false);
		add(remindersLabel);
	}

	public void setEvents(List<Event> events) {

		String remeindingsForTheEvent = "";
		for (int id = 0; id < events.size(); id++) {
			if (events.get(id).getDescription() != null) {
				
				remeindingsForTheEvent += events.get(id).getSummary() + " ";
				remeindingsForTheEvent += events.get(id).getDescription();
				remeindingsForTheEvent += "<br>";
			}
		}
		
		if (!events.isEmpty()) {
			{
				for (int i = 0; i < events.size(); i++) {
					
					if (events.get(i).getSummary().contains("Volleyball")||events.get(i).getSummary().contains("Volleyballspiel")) {
						remeindingsForTheEvent += "Denke an deine Volleyballsachen.";
						remeindingsForTheEvent += "<br>";
					}
					if (events.get(i).getSummary().contains("Fussball")) {
						remeindingsForTheEvent += "Denke an deine Fussballsachen.";
						remeindingsForTheEvent += "<br>";
					}
					if (events.get(i).getSummary().contains("Turnen")) {
						remeindingsForTheEvent += "Denke an deinen Turnbeutel.";
						remeindingsForTheEvent += "<br>";
					}
					if (events.get(i).getSummary().contains("Sport")) {
						remeindingsForTheEvent += "Denke an deine Sportsachen.";
						remeindingsForTheEvent += "<br>";
					}
					if (events.get(i).getSummary().contains("Wandern")) {
						remeindingsForTheEvent += "Denke an deine Wandersachen.";
						remeindingsForTheEvent += "<br>";
					}
				}
			}

		}
		if (remeindingsForTheEvent.isEmpty()) {
			remindersLabel.setText("");
		} else {
			String text = "<html><body>nicht vergessen f�r die Termine: <br>" + remeindingsForTheEvent + "</body></html>";
			remindersLabel.setText(text);
		}

	}
}
