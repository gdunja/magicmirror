package view;

import java.awt.Color;
import java.awt.Font;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class DateTimePanel extends JPanel implements Runnable {
	private JLabel timeLabel;
	private Thread runner;
	private LocalDateTime dateTime;

	public DateTimePanel() {
		timeLabel = new JLabel();
		setOpaque(false);
		timeLabel.setForeground(Color.WHITE);
		timeLabel.setFont(new Font("Arial", Font.BOLD, 40));
		add(timeLabel);

		this.runner = new Thread(this);
		this.runner.start();
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;

	}

	public void run() {
		while (true) {
			this.dateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
			String formatDateTime = dateTime.format(formatter);
			this.timeLabel.setText(formatDateTime.toString());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
				// TODO: handle exception
			}
		}
	}
}
