package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.Locale;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import view.layout.FixedTileLayout;

public class CalendarPanel extends JPanel {

	private JPanel monthPanel;
	private JPanel headerPanel;
	private JPanel dayPanel;

	public CalendarPanel() {
		setLayout(new BorderLayout());
		setOpaque(false);
		setBorder(new EmptyBorder(0, 10, 0, 10));

		monthPanel = new JPanel();
		monthPanel.setOpaque(false);
		add(monthPanel, BorderLayout.NORTH);
		setMonthPanel();

		headerPanel = new JPanel();
		headerPanel.setLayout(new FixedTileLayout(DayOfWeek.values().length, 1, 35, 10));
		headerPanel.setOpaque(false);
		add(headerPanel, BorderLayout.CENTER);

		fillHeadePanel();

		dayPanel = new JPanel();
		dayPanel.setOpaque(false);
		dayPanel.setLayout(new FixedTileLayout(DayOfWeek.values().length, 6, 35, 10));
		add(dayPanel, BorderLayout.SOUTH);

		setDate(LocalDate.now());
	}

	private void fillHeadePanel() {
		for (int i = 0; i < DayOfWeek.values().length; i++) {
			DayOfWeek dayOfWeek = DayOfWeek.values()[i];

			String dayName = dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.getDefault());

			JLabel dayLabel = new JLabel(dayName);
			dayLabel.setVerticalAlignment(JLabel.CENTER);
			dayLabel.setHorizontalAlignment(JLabel.CENTER);
			dayLabel.setForeground(Color.WHITE);

			headerPanel.add(dayLabel, new FixedTileLayout.Index(i, 0));
		}
	}

	public void setDate(LocalDate reference) {
		dayPanel.removeAll();

		LocalDate date = LocalDate.of(reference.getYear(), reference.getMonth(), 1);

		int daysInMonth = date.lengthOfMonth();
		int offset = date.getDayOfWeek().ordinal();

		for (int i = 0; i < daysInMonth; i++) {
			JLabel dayLabel = new JLabel(String.valueOf(i + 1));
			dayLabel.setVerticalAlignment(JLabel.CENTER);
			dayLabel.setHorizontalAlignment(JLabel.CENTER);

			if (date.equals(reference)) {
				dayLabel.setOpaque(true);
				dayLabel.setBackground(Color.WHITE);
				dayLabel.setForeground(Color.BLACK);
			} else {
				dayLabel.setBackground(Color.BLACK);
				dayLabel.setForeground(Color.WHITE);
			}

			int column = date.getDayOfWeek().ordinal();
			int row = (offset + date.getDayOfMonth() - 1) / 7;

			dayPanel.add(dayLabel, new FixedTileLayout.Index(column, row));

			date = date.plusDays(1);
		}

		revalidate();
		repaint();
	}

	public void setMonthPanel() {
		JLabel monthLabel = new JLabel("" + LocalDate.now().getMonth());
		monthLabel.setForeground(Color.WHITE);
		monthPanel.add(monthLabel);
	}
}
