package view.layout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.ArrayList;
import java.util.List;

public class TileLayout implements LayoutManager2 {

    private int tileSize;
    private int gap;

    private int prefColumns = 1;

    private final List<Component> managedChildren = new ArrayList<>();

    public TileLayout(final int tileSize, final int gap, final int prefColumns) {
        this.tileSize = tileSize;
        this.gap = gap;
        this.prefColumns = prefColumns > 0 ? prefColumns : 1;
    }

    @Override
    public void addLayoutComponent(final String name, final Component comp) {
        managedChildren.add(comp);
    }

    @Override
    public void removeLayoutComponent(final Component comp) {
        managedChildren.remove(comp);
    }

    @Override
    public void addLayoutComponent(final Component comp, final Object constraints) {
        managedChildren.add(comp);
    }

    @Override
    public void layoutContainer(final Container target) {
        final Insets insets = target.getInsets();

        final int width = target.getWidth() - insets.left - insets.right;

        int columns = 1;

        for (int currentWidth = tileSize;; columns++) {
            currentWidth += gap + tileSize;

            if (currentWidth > width) {
                break;
            }
        }

        int rows = getRowsForColumns(columns);

        //int offsetX = (width - getAccumulatedLength(columns)) / 2;

        int x = insets.left;
        int y = insets.top;
        int n = 0;

        loop:
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                final Component component = managedChildren.get(n++);

                component.setBounds(x, y, tileSize, tileSize);

                x += tileSize + gap;

                if (n >= managedChildren.size()) {
                    break loop;
                }
            }
            x = insets.left;
            y += tileSize + gap;
        }
    }

    private int getRowsForColumns(final int columns) {
        return managedChildren.size() / columns + (managedChildren.size() % columns > 0 ? 1 : 0);
    }

    private int getAccumulatedLength(final int major) {
        final int minor = major > 0 ? major - 1 : 0;
        return major * tileSize + minor * gap;
    }

    @Override
    public Dimension preferredLayoutSize(final Container target) {
        final Insets insets = target.getInsets();

        int rows = getRowsForColumns(prefColumns);

        int width = getAccumulatedLength(prefColumns);
        int height = getAccumulatedLength(rows);

        width += insets.left + insets.right;
        height += insets.top + insets.bottom;

        return new Dimension(width, height);
    }

    @Override
    public Dimension minimumLayoutSize(final Container target) {
        return preferredLayoutSize(target);
    }

    @Override
    public Dimension maximumLayoutSize(final Container target) {
        return preferredLayoutSize(target);
    }

    @Override
    public float getLayoutAlignmentX(final Container target) {
        return 0.5f;
    }

    @Override
    public float getLayoutAlignmentY(final Container target) {
        return 0.5f;
    }

    @Override
    public void invalidateLayout(final Container target) {

    }
}
