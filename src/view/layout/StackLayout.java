package view.layout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

public class StackLayout implements LayoutManager2 {

    public static final String BOTTOM = "bottom";
    public static final String TOP = "top";

    private List<Component> components = new ArrayList<>();

    public void addLayoutComponent(final Component comp, final Object constraints) {
        synchronized (comp.getTreeLock()) {
            if (BOTTOM.equals(constraints)) {
                components.add(0, comp);
            } else if (TOP.equals(constraints)) {
                components.add(comp);
            } else {
                components.add(comp);
            }
        }
    }

    public void addLayoutComponent(final String name, final Component comp) {
        addLayoutComponent(comp, TOP);
    }

    public void removeLayoutComponent(final Component comp) {
        synchronized (comp.getTreeLock()) {
            components.remove(comp);
        }
    }

    public float getLayoutAlignmentX(final Container target) {
        return 0.5f;
    }

    public float getLayoutAlignmentY(final Container target) {
        return 0.5f;
    }

    public void invalidateLayout(final Container target) {
    }

    public Dimension preferredLayoutSize(final Container parent) {
        synchronized (parent.getTreeLock()) {
            int width = 0;
            int height = 0;

            // Optimised for ArrayList
            for (int i = 0, length = components.size(); i < length; i++) {
                final Component comp = components.get(i);

                Dimension size = LayoutUtils.minPrefMax(comp);
                width = Math.max(size.width, width);
                height = Math.max(size.height, height);
            }

            Insets insets = parent.getInsets();
            width += insets.left + insets.right;
            height += insets.top + insets.bottom;

            return new Dimension(width, height);
        }
    }

    public Dimension minimumLayoutSize(final Container parent) {
        synchronized (parent.getTreeLock()) {
            int width = 0;
            int height = 0;

            // Optimised for ArrayList
            for (int i = 0, length = components.size(); i < length; i++) {
                final Component comp = components.get(i);

                Dimension size = comp.getMinimumSize();
                width = Math.max(size.width, width);
                height = Math.max(size.height, height);
            }

            Insets insets = parent.getInsets();
            width += insets.left + insets.right;
            height += insets.top + insets.bottom;

            return new Dimension(width, height);
        }
    }

    public Dimension maximumLayoutSize(final Container target) {
        return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public void layoutContainer(final Container parent) {
        synchronized (parent.getTreeLock()) {
            final int width = parent.getWidth();
            final int height = parent.getHeight();

            final Insets insets = parent.getInsets();
            final Rectangle bounds = new Rectangle(insets.left, insets.top, width - insets.left - insets.right, height - insets.top - insets.bottom);

            // Optimised for ArrayList
            for (int i = 0, length = components.size(); i < length; i++) {
                final Component comp = components.get(i);

                final Dimension prefSize = LayoutUtils.minPrefMax(comp);

                final int ox = bounds.width > prefSize.width ? bounds.width - prefSize.width : 0;
                final int oy = bounds.height > prefSize.height ? bounds.height - prefSize.height : 0;

                comp.setBounds(bounds.x + ox / 2, bounds.y + oy / 2, bounds.width - ox, bounds.height - oy);
                parent.setComponentZOrder(comp, length - i - 1);
            }
        }
    }
}
