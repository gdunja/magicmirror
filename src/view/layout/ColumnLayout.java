package view.layout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;

public class ColumnLayout implements LayoutManager2 {

    private int gap;

    public ColumnLayout() {
    }

    public ColumnLayout(final int gap) {
        this.gap = gap;
    }

    @Override
    public void addLayoutComponent(final Component comp, final Object constraints) {

    }

    @Override
    public Dimension maximumLayoutSize(final Container target) {
        final Insets insets = target.getInsets();

        int width = 0;
        int height = 0;

        final Component[] components = target.getComponents();

        for (int n = 0, length = components.length; n < length; n++) {
            final Component component = components[n];

            final Dimension dimension = component.getMaximumSize();

            width = Math.max(width, dimension.width);
            height = Math.max(height, dimension.height);
        }

        return new Dimension(insets.left + width + getAccumulatedGap(components) + insets.right, insets.top + height + insets.bottom);
    }

    @Override
    public float getLayoutAlignmentX(final Container target) {
        return 0.5f;
    }

    @Override
    public float getLayoutAlignmentY(final Container target) {
        return 0.5f;
    }

    @Override
    public void invalidateLayout(final Container target) {

    }

    @Override
    public void addLayoutComponent(final String name, final Component comp) {

    }

    @Override
    public void removeLayoutComponent(final Component comp) {

    }

    @Override
    public Dimension preferredLayoutSize(final Container parent) {
        final Insets insets = parent.getInsets();

        int width = 0;
        int height = 0;

        final Component[] components = parent.getComponents();

        for (int n = 0, length = components.length; n < length; n++) {
            final Component component = components[n];

            final Dimension dimension = component.getPreferredSize();

            width = Math.max(width, dimension.width);
            height = Math.max(height, dimension.height);
        }

        return new Dimension(insets.left + width + getAccumulatedGap(components) + insets.right, insets.top + height + insets.bottom);
    }

    @Override
    public Dimension minimumLayoutSize(final Container parent) {
        final Insets insets = parent.getInsets();

        int width = 0;
        int height = 0;

        final Component[] components = parent.getComponents();

        for (int n = 0, length = components.length; n < length; n++) {
            final Component component = components[n];

            final Dimension dimension = component.getMinimumSize();

            width = Math.max(width, dimension.width);
            height = Math.max(height, dimension.height);
        }

        return new Dimension(insets.left + width + getAccumulatedGap(components) + insets.right, insets.top + height + insets.bottom);
    }

    private int getAccumulatedGap(final Component[] components) {
        return (components.length - 1) * gap;
    }

    @Override
    public void layoutContainer(final Container parent) {
        final Component[] components = parent.getComponents();

        final Insets insets = parent.getInsets();

        final int width = parent.getWidth() - insets.left - insets.right;

        final int columnWidth = (width - getAccumulatedGap(components)) / components.length;
        final int reducedHeight = parent.getHeight() - insets.top - insets.bottom;

        int offsetX = insets.left;

        for (int n = 0, length = components.length; n < length; n++) {
            final Component component = components[n];

            component.setBounds(offsetX, insets.top, columnWidth, reducedHeight);

            offsetX += columnWidth + gap;
        }
    }
}
