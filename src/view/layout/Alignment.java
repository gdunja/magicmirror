package view.layout;

public enum Alignment {

    TOP_LEFT(VAlign.TOP, HAlign.LEFT),

    TOP_CENTER(VAlign.TOP, HAlign.CENTER),

    TOP_RIGHT(VAlign.TOP, HAlign.RIGHT),

    CENTER_LEFT(VAlign.CENTER, HAlign.LEFT),

    CENTER(VAlign.CENTER, HAlign.CENTER),

    CENTER_RIGHT(VAlign.CENTER, HAlign.RIGHT),

    BOTTOM_LEFT(VAlign.BOTTOM, HAlign.LEFT),

    BOTTOM_CENTER(VAlign.BOTTOM, HAlign.CENTER),

    BOTTOM_RIGHT(VAlign.BOTTOM, HAlign.RIGHT),

    BASELINE_LEFT(VAlign.BASELINE, HAlign.LEFT),

    BASELINE_CENTER(VAlign.BASELINE, HAlign.CENTER),

    BASELINE_RIGHT(VAlign.BASELINE, HAlign.RIGHT);

    private final VAlign vAlign;
    private final HAlign hAlign;

    Alignment(final VAlign vAlign, final HAlign hAlign) {
        this.vAlign = vAlign;
        this.hAlign = hAlign;
    }

    public VAlign getVAlign() {
        return vAlign;
    }

    public HAlign getHAlign() {
        return hAlign;
    }
}
