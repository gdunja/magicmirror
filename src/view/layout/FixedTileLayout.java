package view.layout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;

public class FixedTileLayout implements LayoutManager2 {

    private Component[][] indices;
    private int columns;
    private int rows;

    private int gap;

    public FixedTileLayout(final int width, final int height, final int tileSize, final int gap) {
        this.tileSize = tileSize;
        this.indices = new Component[width][height];
        this.columns = width;
        this.rows = height;
        this.gap = gap;
    }

    @Override
    public void addLayoutComponent(final Component comp, final Object constraints) {
        if (constraints != null && constraints instanceof Index) {
            final Index index = (Index) constraints;

            ensureCapacity(index);

            indices[index.x][index.y] = comp;
            return;
        }

        throw new IllegalArgumentException();
    }

    private void ensureCapacity(final Index index) {
        if (index.x > indices.length) {
            final Component[][] newIndices = new Component[index.x][index.y];
            System.arraycopy(indices, 0, newIndices, 0, indices.length);
            indices = newIndices;
        }
    }

    @Override
    public Dimension maximumLayoutSize(final Container target) {
        return preferredLayoutSize(target);
    }

    @Override
    public float getLayoutAlignmentX(final Container target) {
        return 0.5f;
    }

    @Override
    public float getLayoutAlignmentY(final Container target) {
        return 0.5f;
    }

    @Override
    public void invalidateLayout(final Container target) {

    }

    @Override
    public void addLayoutComponent(final String name, final Component comp) {

    }

    @Override
    public void removeLayoutComponent(final Component comp) {
        for (int column = 0, columns = indices.length; column < columns; column++) {
            final Component[] array = indices[column];

            for (int row = 0, rows = array.length; row < rows; row++) {
                final Component component = array[row];

                if (component == comp) {
                    array[row] = null;
                }
            }
        }
    }

    @Override
    public Dimension preferredLayoutSize(final Container parent) {
        final Insets insets = parent.getInsets();

        final int requiredWidth = columns * tileSize + (columns - 1) * gap;
        final int requiredHeight = rows * tileSize + (rows - 1) * gap;

        return new Dimension(insets.left + insets.right + requiredWidth, insets.top + insets.right + requiredHeight);
    }

    @Override
    public Dimension minimumLayoutSize(final Container parent) {
        return preferredLayoutSize(parent);
    }

    private int tileSize = 40;

    @Override
    public void layoutContainer(final Container parent) {
        final int requiredWidth = columns * tileSize + (columns - 1) * gap;
        final int requiredHeight = rows * tileSize + (rows - 1) * gap;

        int ox = (parent.getWidth() - requiredWidth) / 2;
        int oy = (parent.getHeight() - requiredHeight) / 2;

        for (int column = 0, columns = indices.length; column < columns; column++) {
            final Component[] array = indices[column];

            for (int row = 0, rows = array.length; row < rows; row++) {
                final Component component = array[row];

                if (component != null) {
                    component.setBounds(ox, oy, tileSize, tileSize);
                }

                oy += tileSize + gap;
            }
            ox += tileSize + gap;
            oy = (parent.getHeight() - requiredHeight) / 2;
        }
    }

    public static final class Index {

        final int x;
        final int y;

        public Index(final int x, final int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Index)) {
                return false;
            }

            Index index = (Index) o;

            if (x != index.x) {
                return false;
            }
            return y == index.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }
    }
}
