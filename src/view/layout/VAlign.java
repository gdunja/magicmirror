package view.layout;
public enum VAlign {

    TOP,

    CENTER,

    BOTTOM,

    BASELINE

}
