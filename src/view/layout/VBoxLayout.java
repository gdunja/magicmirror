package view.layout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.List;

public class VBoxLayout extends AligningLayout {

    private int space;

    public VBoxLayout() {
        this(Alignment.TOP_LEFT, 0);
    }

    public VBoxLayout(final Alignment alignment) {
        this(alignment, 0);
    }

    public VBoxLayout(final int space) {
        this(Alignment.TOP_LEFT, space);
    }

    public VBoxLayout(final Alignment alignment, final int space) {
        setAlignment(alignment);
        this.space = space < 0 ? 0 : space;
    }

    @Override
    public void layoutContainer(final Container parent) {
        final List<Component> managedChildren = getManagedChildren();

        final Alignment alignment = getAlignment();
        final VAlign vAlign = alignment.getVAlign();
        final HAlign hAlign = alignment.getHAlign();

        final Insets insets = getInsets(parent);
        final int top = insets.top;
        final int left = insets.left;
        final int bottom = insets.bottom;
        final int right = insets.right;

        final int width = parent.getWidth();
        final int height = parent.getHeight();

        int contentWidth = width - left - right;
        int contentHeight = fastComputeSpaceWidth(managedChildren);

        // This is the reduced version in which the overhead was shrink
        // down to 2n instead of 3n for pre-computation and computation.
        // This loop computes the child minPrefMax an the contentHeight.
        final Dimension[] sizes = new Dimension[managedChildren.size()];
        for (int i = 0, length = managedChildren.size(); i < length; i++) {
            final Component child = managedChildren.get(i);

            final Dimension size = LayoutUtils.minPrefMax(child);

            contentHeight += size.height;

            sizes[i] = size;
        }

        // int x = left;
        int y = top + computeYOffset(height - top - bottom, contentHeight, vAlign);

        for (int i = 0, length = managedChildren.size(); i < length; i++) {
            final Component child = managedChildren.get(i);
            final Dimension size = sizes[i];

            int childWidth = computeChildBound(size.width, contentWidth);
            int xOffset = left + computeXOffset(contentWidth, childWidth, hAlign);

            child.setBounds(xOffset, y, childWidth, size.height);

            y += size.height + space;
        }
    }

    private int fastComputeSpaceWidth(final List<Component> children) {
        return children.isEmpty() ? 0 : space * (children.size() - 1);
    }

    @Override
    public Dimension minimumLayoutSize(final Container parent) {
        final List<Component> managedChildren = getManagedChildren();
        final Insets insets = getInsets(parent);

        int width = 0;
        int height = insets.top + insets.bottom + fastComputeSpaceWidth(managedChildren);

        for (int i = 0, length = managedChildren.size(); i < length; i++) {
            final Dimension min = managedChildren.get(i).getMinimumSize();

            width = Math.max(width, min.width);
            height += min.height;
        }

        return new Dimension(width + insets.left + insets.bottom, height);
    }

    @Override
    public Dimension preferredLayoutSize(final Container parent) {
        final List<Component> managedChildren = getManagedChildren();
        final Insets insets = getInsets(parent);

        final Dimension[] childrenSizes = computeChildrenSizes(managedChildren);

        int width = 0;
        int height = insets.top + insets.bottom + fastComputeSpaceWidth(managedChildren);

        for (int i = 0, length = managedChildren.size(); i < length; i++) {
            width = Math.max(width, childrenSizes[i].width);
            height += childrenSizes[i].height;
        }

        return new Dimension(width + insets.left + insets.bottom, height);
    }

    @Override
    public Dimension maximumLayoutSize(final Container target) {
        final List<Component> managedChildren = getManagedChildren();
        final Insets insets = getInsets(target);

        int width = 0;
        int height = insets.top + insets.bottom + fastComputeSpaceWidth(managedChildren);

        for (int i = 0, length = managedChildren.size(); i < length; i++) {
            final Dimension min = managedChildren.get(i).getMaximumSize();

            width = Math.max(width, min.width);
            height += min.height;
        }

        return new Dimension(width + insets.left + insets.bottom, height);
    }
}
