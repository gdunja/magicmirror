package view.layout;

import java.awt.Component;
import java.awt.Dimension;

public class LayoutUtils {

	public static Dimension minPrefMax(final Component component) {
		final Dimension min = component.getMinimumSize();
		final Dimension pref = component.getPreferredSize();
		final Dimension max = component.getMaximumSize();

		int width = Math.min(Math.max(pref.width, min.width), Math.max(min.width, max.width));
		int height = Math.min(Math.max(pref.height, min.height), Math.max(min.height, max.height));

		return new Dimension(width, height);
	}

}
