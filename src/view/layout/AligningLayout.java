package view.layout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public abstract class AligningLayout implements LayoutManager2 {

    private final List<Component> managedChildren = new ArrayList<>();
    private List<Component> managedChildrenUnmodifiable;

    protected List<Component> getManagedChildren() {
        if (managedChildrenUnmodifiable == null) {
            managedChildrenUnmodifiable = Collections.unmodifiableList(managedChildren);
        }
        return managedChildrenUnmodifiable;
    }

    @Override
    public void addLayoutComponent(final Component comp, final Object constraints) {
        managedChildren.add(comp);
    }

    @Override
    public void addLayoutComponent(final String name, final Component comp) {
        addLayoutComponent(comp, null);
    }

    @Override
    public void removeLayoutComponent(final Component comp) {
        managedChildren.remove(comp);
    }

    private Alignment alignment = Alignment.TOP_LEFT;

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(final Alignment alignment) {
        this.alignment = Objects.requireNonNull(alignment);
    }

    protected int computeXOffset(final int width, final int contentWidth, final HAlign hAlign) {
        switch (hAlign) {
            case LEFT:
                return 0;
            case CENTER:
                return (width - contentWidth) / 2;
            case RIGHT:
                return (width - contentWidth);
            default:
                throw new IllegalStateException();
        }
    }

    protected int computeYOffset(final int height, final int contentHeight, final VAlign vAlign) {
        switch (vAlign) {
            case TOP:
                return 0;
            case CENTER:
                return (height - contentHeight) / 2;
            case BOTTOM:
                return (height - contentHeight);
            case BASELINE:
                return 0;
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public float getLayoutAlignmentX(final Container target) {
        switch (alignment.getHAlign()) {
            case LEFT:
                return 0;
            case CENTER:
                return 0.5f;
            case RIGHT:
                return 1;
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public float getLayoutAlignmentY(final Container target) {
        switch (alignment.getVAlign()) {
            case TOP:
                return 0;
            case CENTER:
                return 0.5f;
            case BOTTOM:
                return 1;
            case BASELINE:
                // This is not really true, but it is the closets to
                // what is meant by it.
                return 0.5f;
            default:
                throw new IllegalStateException();
        }
    }

    private Insets insets;

    protected Insets getInsets(final Container container) {
        final Insets borderInsets = container.getInsets();

        if (insets != null) {
            return new Insets(insets.top + borderInsets.top, insets.left + borderInsets.left, insets.bottom + insets.bottom, insets.right + insets.right);
        }

        return borderInsets;
    }

    protected Dimension[] computeChildrenSizes(final List<Component> children) {
        final Dimension[] sizes = getTempSizeArray(children.size());

        for (int i = 0, length = children.size(); i < length; i++) {
            sizes[i] = LayoutUtils.minPrefMax(children.get(i));
        }

        return sizes;
    }

    private Dimension[] tempSizeArray;

    private Dimension[] getTempSizeArray(final int length) {
        if (tempSizeArray == null || tempSizeArray.length < length) {
            tempSizeArray = new Dimension[length];
        }
        return tempSizeArray;
    }

    private boolean restrictToBounds;

    public boolean isRestrictToBounds() {
        return restrictToBounds;
    }

    public void setRestrictToBounds(final boolean restrictToBounds) {
        this.restrictToBounds = restrictToBounds;
    }

    protected int computeChildBound(final int size, final int contentSize) {
        return isRestrictToBounds() && size > contentSize ? contentSize : size;
    }

    @Override
    public void invalidateLayout(final Container target) {

    }

}
