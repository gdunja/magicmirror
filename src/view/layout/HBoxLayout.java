package view.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.List;
import java.util.function.Function;

public class HBoxLayout extends AligningLayout {

	private int space;

	public HBoxLayout() {
		this(Alignment.TOP_LEFT, 0);
	}

	public HBoxLayout(final Alignment alignment) {
		this(alignment, 0);
	}

	public HBoxLayout(final int space) {
		this(Alignment.TOP_LEFT, space);
	}

	public HBoxLayout(final Alignment alignment, final int space) {
		setAlignment(alignment);
		this.space = space < 0 ? 0 : space;
	}

	/**
	 * Performance: 2n
	 */
	@Override
	public void layoutContainer(final Container parent) {
		final List<Component> managedChildren = getManagedChildren();
		final Insets insets = getInsets(parent);
		final Alignment alignment = getAlignment();
		final VAlign vAlign = alignment.getVAlign();
		final HAlign hAlign = alignment.getHAlign();

		int top = insets.top;
		int left = insets.left;
		int bottom = insets.bottom;
		int right = insets.right;

		final int width = parent.getWidth();
		final int height = parent.getHeight();

		int contentWidth = fastComputeSpaceWidth(managedChildren);
		int contentHeight = height - top - bottom;

		int baselineOffset = -1;
		final boolean computeBaseline = vAlign == VAlign.BASELINE;
		final Dimension[] sizes = new Dimension[managedChildren.size()];

		// This is the reduced version in which the overhead was shrink
		// down to 2n instead of 4n for pre-computation and computation.
		// This loop computes the child minPrefMax, the contentWidth and
		// the baseline offset.
		for (int i = 0, length = managedChildren.size(); i < length; i++) {
			final Component child = managedChildren.get(i);

			final Dimension size = LayoutUtils.minPrefMax(child);
			contentWidth += size.width;
			sizes[i] = size;

			if (computeBaseline) {
				int baseline = child.getBaseline(size.width, size.height);

				if (baseline >= 0) {
					baselineOffset = Math.max(baseline, baselineOffset);
				}
			}
		}

		// int y = top;
		int x = left + computeXOffset(width - left - right, contentWidth, hAlign);

		for (int i = 0, length = managedChildren.size(); i < length; i++) {
			final Component child = managedChildren.get(i);
			final Dimension size = sizes[i];

			final int yOffset;
			if (baselineOffset == -1) {
				yOffset = top + computeYOffset(contentHeight, size.height, vAlign);
			} else {
				final int baseline = child.getBaseline(size.width, size.height);

				if (baseline >= 0) {
					yOffset = top + (baselineOffset - baseline);
				} else {
					yOffset = top;
				}
			}

			final int childHeight = computeChildBound(size.height, contentHeight);

			child.setBounds(x, yOffset, size.width, childHeight);

			x += space + size.width;
		}
	}

	private int computeBaselineOffset(final List<Component> children,
			final Function<Component, Dimension> sizeFunction) {
		int baselineOffset = -1;

		if (getAlignment().getVAlign() == VAlign.BASELINE) {
			return baselineOffset;
		}

		for (int i = 0, length = children.size(); i < length; i++) {
			final Component child = children.get(i);
			final Dimension size = sizeFunction.apply(child);

			int baseline = child.getBaseline(size.width, size.height);

			if (baseline >= 0) {
				baselineOffset = Math.max(baseline, baselineOffset);
			}
		}

		return baselineOffset;
	}

	private int fastComputeSpaceWidth(final List<Component> children) {
		return children.isEmpty() ? 0 : space * (children.size() - 1);
	}

	/**
	 * Performance: 2n for VAlign=BOTTOM, 1n otherwise
	 */
	@Override
	public Dimension minimumLayoutSize(final Container parent) {
		final List<Component> managedChildren = getManagedChildren();
		final Insets insets = getInsets(parent);

		final int baselineOffset = computeBaselineOffset(managedChildren, Component::getMinimumSize);

		int width = insets.left + insets.right + fastComputeSpaceWidth(managedChildren);
		int height = 0;

		for (int i = 0, length = managedChildren.size(); i < length; i++) {
			final Component child = managedChildren.get(i);
			final Dimension min = child.getMinimumSize();

			width += min.width;
			height = Math.max(height, min.height);

			if (baselineOffset > 0) {
				final int baseline = child.getBaseline(min.width, min.height);

				if (baseline > 0) {
					height = Math.max(height, min.height + (baselineOffset - baseline));
				}
			}
		}

		return new Dimension(width, height + insets.top + insets.bottom);
	}

	@Override
	public Dimension preferredLayoutSize(final Container parent) {
		final List<Component> managedChildren = getManagedChildren();
		final Insets insets = getInsets(parent);

		int baselineOffset = -1;
		final boolean computeBaseline = getAlignment().getVAlign() == VAlign.BASELINE;
		final Dimension[] sizes = new Dimension[managedChildren.size()];

		for (int i = 0, length = managedChildren.size(); i < length; i++) {
			final Component child = managedChildren.get(i);

			final Dimension size = LayoutUtils.minPrefMax(child);
			sizes[i] = size;

			if (computeBaseline) {
				int baseline = child.getBaseline(size.width, size.height);

				if (baseline >= 0) {
					baselineOffset = Math.max(baseline, baselineOffset);
				}
			}
		}

		int width = insets.left + insets.right + fastComputeSpaceWidth(managedChildren);
		int height = 0;

		for (int i = 0, length = managedChildren.size(); i < length; i++) {
			final Component child = managedChildren.get(i);
			final Dimension size = sizes[i];

			width += size.width;
			height = Math.max(height, size.height);

			if (baselineOffset > 0) {
				final int baseline = child.getBaseline(size.width, size.height);

				if (baseline > 0) {
					height = Math.max(height, size.height + (baselineOffset - baseline));
				}
			}
		}

		return new Dimension(width, height + insets.top + insets.bottom);
	}

	/**
	 * Performance: 2n for VAlign=BOTTOM, 1n otherwise
	 */
	@Override
	public Dimension maximumLayoutSize(final Container target) {
		final List<Component> managedChildren = getManagedChildren();
		final Insets insets = getInsets(target);

		final int baselineOffset = computeBaselineOffset(managedChildren, Component::getMaximumSize);

		int width = insets.left + insets.right + fastComputeSpaceWidth(managedChildren);
		int height = 0;

		for (int i = 0, length = managedChildren.size(); i < length; i++) {
			final Component child = managedChildren.get(i);
			final Dimension max = child.getMaximumSize();

			width += max.width;
			height = Math.max(height, max.height);

			if (baselineOffset > 0) {
				final int baseline = child.getBaseline(max.width, max.height);

				if (baseline > 0) {
					height = Math.max(height, max.height + (baselineOffset - baseline));
				}
			}
		}

		return new Dimension(width, height + insets.top + insets.bottom);
	}
}
