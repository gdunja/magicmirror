package view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class AgePanel extends JPanel {
	private JLabel ageLabel;

	public AgePanel() {
		setOpaque(false);
		ageLabel = new JLabel();
		ageLabel.setForeground(Color.WHITE);
		ageLabel.setFont(new Font("Arial", Font.BOLD, 15));
		add(ageLabel);
	}

	public void setAge(int age) {
		ageLabel.setText("Du siehst heute " + age+" Jahre alt aus.");

	}
}
