package view;
//This sample uses Apache HttpComponents:

//http://hc.apache.org/httpcomponents-core-ga/httpcore/apidocs/
//https://hc.apache.org/httpcomponents-client-ga/httpclient/apidocs/

import java.io.File;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import de.grimmig.smartmirror.domain.HairColor;
import de.grimmig.smartmirror.domain.Person;

public class Main {
	// Replace <Subscription Key> with your valid subscription key.
	private static final String subscriptionKey = "483342f07d2e4552a32e211773a30f51";

	// NOTE: You must use the same region in your REST call as you used to
	// obtain your subscription keys. For example, if you obtained your
	// subscription keys from westus, replace "westcentralus" in the URL
	// below with "westus".
	//
	// Free trial subscription keys are generated in the "westus" region. If you
	// use a free trial subscription key, you shouldn't need to change this region.
	private static final String uriBase = "https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect";

	// private static final String imageWithFaces =
	// "C:\\Users\\Dunja\\workspaceub\\FirstOpenCV\\NeuerOrdner\\test2.jpg";

	private static final String faceAttributes = "age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";

	public void getAzureInformation(String imageWithFaces, Content content, Person person) {
		System.out.println("azure main klasse funktioniert");
		// TODO Auto-generated method stub
		HttpClient httpclient = HttpClientBuilder.create().build();

		try {
			URIBuilder builder = new URIBuilder(uriBase);

			// Request parameters. All of them are optional.
			builder.setParameter("returnFaceId", "true");
			builder.setParameter("returnFaceLandmarks", "false");
			builder.setParameter("returnFaceAttributes", faceAttributes);

			// Prepare the URI for the REST API call.
			URI uri = builder.build();
			HttpPost request = new HttpPost(uri);

			// Request headers.
			request.setHeader("Content-Type", "application/octet-stream");
			request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);

			// Request body.
			File file = new File(imageWithFaces);
			FileEntity reqEntity = new FileEntity(file, ContentType.APPLICATION_OCTET_STREAM);
			request.setEntity(reqEntity);

			// Execute the REST API call and get the response entity.
			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				// Format and display the JSON response.

				String jsonString = EntityUtils.toString(entity).trim();
				if (jsonString.charAt(0) == '[') {
					JSONArray jsonArray = new JSONArray(jsonString);
					JSONObject jOkomplett = jsonArray.getJSONObject(0);
					JSONObject jOfaceAttributes = jOkomplett.getJSONObject("faceAttributes");

					JSONObject jOmakeup = jOfaceAttributes.getJSONObject("makeup");
					boolean jeyeMakeup = Boolean.valueOf(jOmakeup.get("eyeMakeup").toString());
					person.setEyemakeup(jeyeMakeup);
					boolean jlipMakeup = Boolean.valueOf(jOmakeup.get("lipMakeup").toString());
					person.setLipmakeup(jlipMakeup);
					JSONObject jOfacialHair = jOfaceAttributes.getJSONObject("facialHair");
					double jsideburns = Double.parseDouble(jOfacialHair.get("sideburns").toString());
					person.getBeard().setSideburns(jsideburns);
					double jbeard = Double.parseDouble(jOfacialHair.get("beard").toString());
					person.getBeard().setBeard(jbeard);
					double jmoustache = Double.parseDouble(jOfacialHair.get("moustache").toString());
					person.getBeard().setMoustache(jmoustache);
					String jAgender = jOfaceAttributes.get("gender").toString();
					person.setGender(jAgender);
					// nachträglich noch hinzufügen der Accessoires
					// JSONObject jOaccessories = jOfaceAttributes.getJSONObject("accessories");
					// String jAsmile = jOfaceAttributes.get("smile").toString();
					String jAglasses = jOfaceAttributes.get("glasses").toString();
					person.setGlases(jAglasses);
					String jAheadware = jOfaceAttributes.get("glasses").toString();
					if (jAheadware != null) {
						person.setHeadware(true);
					}

					JSONObject jOhair = jOfaceAttributes.getJSONObject("hair");
					JSONArray jOhaircolor = jOhair.getJSONArray("hairColor");
					if (jOhaircolor != null && jOhaircolor.length() > 0) {
						person.getHairColor().setBrown(
								Double.parseDouble(jOhaircolor.getJSONObject(0).get("confidence").toString()));
						person.getHairColor().setBlack(
								Double.parseDouble(jOhaircolor.getJSONObject(1).get("confidence").toString()));
						person.getHairColor().setOther(
								Double.parseDouble(jOhaircolor.getJSONObject(2).get("confidence").toString()));
						person.getHairColor().setBlond(
								Double.parseDouble(jOhaircolor.getJSONObject(3).get("confidence").toString()));
						person.getHairColor()
								.setRed(Double.parseDouble(jOhaircolor.getJSONObject(4).get("confidence").toString()));
						person.getHairColor()
								.setGray(Double.parseDouble(jOhaircolor.getJSONObject(5).get("confidence").toString()));

					}
					else {
						person.getHairColor().setNoHair(true);
					}
					JSONObject jOemotion = jOfaceAttributes.getJSONObject("emotion");
					person.getExpression().setContempt(Double.parseDouble(jOemotion.get("contempt").toString()));
					person.getExpression().setSurprise(Double.parseDouble(jOemotion.get("surprise").toString()));
					person.getExpression().setHappiness(Double.parseDouble(jOemotion.get("happiness").toString()));
					person.getExpression().setNeutral(Double.parseDouble(jOemotion.get("neutral").toString()));
					person.getExpression().setSadness(Double.parseDouble(jOemotion.get("sadness").toString()));
					person.getExpression().setDisgust(Double.parseDouble(jOemotion.get("disgust").toString()));
					person.getExpression().setAnger(Double.parseDouble(jOemotion.get("anger").toString()));
					person.getExpression().setFear(Double.parseDouble(jOemotion.get("fear").toString()));
					double jAage = Double.parseDouble(jOfaceAttributes.get("age").toString());
					person.setAge(jAage);

				} else if (jsonString.charAt(0) == '{') {
					JSONObject jsonObject = new JSONObject(jsonString);

					System.out.println("json String:"+jsonObject.toString(2));

				} else {
					System.out.println("json String:"+jsonString);
				}
			}
		} catch (Exception e) {
			// Display error message.
			System.out.println(e.getMessage());
		}

	}

}
