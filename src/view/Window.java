package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import de.grimmig.smartmirror.domain.Person;
import de.grimmig.smartmirror.weather.Weather;
import view.layout.Alignment;
import view.layout.VBoxLayout;

public class Window implements KeyListener {
	private JFrame frame;

	private DateTimePanel dateTimePanel;
	private WeatherPanel weatherPanel;
	private CalendarPanel calendarPanel;

	private GreetingsPanel greetingsPanel;

	private EventsPanel eventsPanel;
	private RemindersPanel remindersPanel;

	private InfosPanel infosPanel;
	private AgePanel agePanel;

	public void showEmptyWindow() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel pangesamt = new JPanel();
		pangesamt.setBackground(Color.BLACK);
		pangesamt.setLayout(new GridLayout(3, 1));

		frame.setContentPane(pangesamt);

		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setUndecorated(true);
		frame.setVisible(true);
		frame.getContentPane().setBackground(Color.BLACK);

		pangesamt.registerKeyboardAction((e) -> {
			frame.dispose();
		}, "", KeyStroke.getKeyStroke("ESCAPE"), JComponent.WHEN_IN_FOCUSED_WINDOW);
	}

	public void closeWindow() {
		frame.dispose();
	}

	public void showWindow(Content content, Weather wetter, Person person) {

		// frame = new JFrame();
		// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel pangesamt = new JPanel();
		pangesamt.setBackground(Color.BLACK);
		pangesamt.setLayout(new GridLayout(3, 1));

		pangesamt.add(buildTopPanel());
		pangesamt.add(buildCenterPanel());
		pangesamt.add(buildBottomPanel());

		frame.setContentPane(pangesamt);

		dateTimePanel.setDateTime(LocalDateTime.now());
		weatherPanel.setWeather(wetter);
		calendarPanel.setDate(LocalDate.now());

		eventsPanel.setEvents(content.getEvents());
		remindersPanel.setEvents(content.getEvents());

		greetingsPanel.setContent(person);

		infosPanel.setContent(content, wetter, person);
		agePanel.setAge((int) person.getAge());

		// frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		// frame.setUndecorated(true);
		// frame.setVisible(true);
		// frame.getContentPane().setBackground(Color.BLACK);
		//
		// pangesamt.registerKeyboardAction((e) -> {
		// frame.dispose();
		// }, "", KeyStroke.getKeyStroke("ESCAPE"), JComponent.WHEN_IN_FOCUSED_WINDOW);
	}

	private Component buildTopPanel() {
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setLayout(new BorderLayout());

		JPanel leftPanel = new JPanel();
		leftPanel.setOpaque(false);
		leftPanel.setLayout(new VBoxLayout(Alignment.TOP_LEFT));
		panel.add(leftPanel, BorderLayout.WEST);

		dateTimePanel = new DateTimePanel();
		leftPanel.add(dateTimePanel);

		weatherPanel = new WeatherPanel();
		leftPanel.add(weatherPanel);

		calendarPanel = new CalendarPanel();
		leftPanel.add(calendarPanel);

		JPanel rightPanel = new JPanel();
		rightPanel.setOpaque(false);
		rightPanel.setLayout(new VBoxLayout(Alignment.TOP_LEFT));
		panel.add(rightPanel, BorderLayout.EAST);

		eventsPanel = new EventsPanel();
		rightPanel.add(eventsPanel);

		remindersPanel = new RemindersPanel();
		rightPanel.add(remindersPanel);

		return panel;
	}

	private Component buildCenterPanel() {
		greetingsPanel = new GreetingsPanel();
		greetingsPanel.setOpaque(false);
		return greetingsPanel;

	}

	private Component buildBottomPanel() {
		JPanel verticalPanel = new JPanel();
		verticalPanel.setOpaque(false);
		verticalPanel.setLayout(new VBoxLayout(Alignment.CENTER));

		infosPanel = new InfosPanel();
		verticalPanel.add(infosPanel);

		agePanel = new AgePanel();
		verticalPanel.add(agePanel);

		return verticalPanel;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		// handler
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			System.out.println("escaped ?");
			System.exit(0);
		} else {
			System.out.println("not escaped");
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
