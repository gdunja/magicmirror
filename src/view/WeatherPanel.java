package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.grimmig.smartmirror.weather.Weather;

public class WeatherPanel extends JPanel {

	private JLabel iconLabel;
	private JLabel statusLabel;
	
	public WeatherPanel() {
		setLayout(new BorderLayout());
		setOpaque(false);
		
		iconLabel = new JLabel();
		iconLabel.setForeground(Color.WHITE);
		add(iconLabel, BorderLayout.WEST);
		
		statusLabel = new JLabel();
		statusLabel.setForeground(Color.WHITE);
		statusLabel.setFont(new Font("Arial", Font.BOLD, 20));
		add(statusLabel,BorderLayout.CENTER);
	}
	
	public void setWeather(Weather weather) {
		ImageIcon img = new ImageIcon("..\\FirstOpenCV\\src\\view\\SimpleWeatherIcons\\" + weather.getIconName() + ".png");
		iconLabel.setIcon(img);
		statusLabel.setText("<html><body>Ort: " + weather.getName() + "<br>Temperatur: " + weather.getNowtemp()
				+ "<br>Min: " + weather.getMinTemp() + "�C Max: " + weather.getMaxTemp() + "�C</body></html>");
	}
	
	
}
