package view;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

public class SwingCalendar {

	DefaultTableModel model;
	Calendar cal = new GregorianCalendar();
	JLabel label;
	JPanel panGesamt = new JPanel();
	SwingCalendar() {

		// this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// this.setTitle("Swing Calandar");
		// this.setSize(300,200);
		// this.setLayout(new BorderLayout());
		// this.setVisible(true);

		
		panGesamt.setLayout(new BorderLayout());
		label = new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(label, BorderLayout.CENTER);

		String[] columns = { "So", "Mo", "Di", "Mi", "Do", "Fr", "Sa" };
		model = new DefaultTableModel(null, columns);
		JTable table = new JTable(model);
		JScrollPane pane = new JScrollPane(table);

		panGesamt.add(panel, BorderLayout.NORTH);
		panGesamt.add(pane, BorderLayout.CENTER);

		this.updateMonth();

	}

	void updateMonth() {
		cal.set(Calendar.DAY_OF_MONTH, 1);

		String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.GERMANY);
		int year = cal.get(Calendar.YEAR);
		label.setText(month + " " + year);

		int startDay = cal.get(Calendar.DAY_OF_WEEK);
		int numberOfDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		int weeks = cal.getActualMaximum(Calendar.WEEK_OF_MONTH);
		System.out.println(weeks);
		model.setRowCount(0);
		model.setRowCount(weeks + 2);

		int i = startDay - 1;
		for (int day = 1; day <= numberOfDays; day++) {
			model.setValueAt(day, i / 7, i % 7);
			i = i + 1;
		}

	}
public Component getPanel() {
	return panGesamt;
}
//	public static void main(String[] arguments) {
//		JFrame.setDefaultLookAndFeelDecorated(true);
//		SwingCalendar sc = new SwingCalendar();
//	}

}