package view;

import java.awt.Color;
import java.awt.Font;
import java.time.LocalDateTime;
import java.util.Random;

import javax.swing.JLabel;
import javax.swing.JPanel;

import de.grimmig.smartmirror.domain.Person;
import de.grimmig.smartmirror.weather.Weather;

public class InfosPanel extends JPanel {
	private JLabel infosLabel;

	public InfosPanel() {
		setOpaque(false);
		infosLabel = new JLabel();
		infosLabel.setForeground(Color.WHITE);
		infosLabel.setFont(new Font("Arial", Font.BOLD, 15));
		add(infosLabel);
	}

	public void setContent(Content content, Weather weather, Person person) {
		// smilesentence is missing
		StringBuilder builder = new StringBuilder();
		getCommentMood(person, builder);
		smilingPersonDetected(content, builder);
		getCommentHaircolor(person, builder);
		getCommentFacialMakeup(person, builder);
		additionalInformations(weather, content, person, builder);
		if (builder.length() == 0) {
			infosLabel.setText("");
		} else {
			infosLabel.setText("<html><body>" + builder.toString() + "</body></html>");
		}
	}

	public void getCommentMood(Person person, StringBuilder builder) {
		builder.append("Es scheint mir, dass du heute ");
		builder.append(person.getExpression().getCommentMood());
		builder.append("bist.<br>");
		if (person.getExpression().getSadness() > 0.6) {
			builder.append("Du siehst so traurig aus, deshalb erz�hle ich dir einen Witz.<br>" + newJoke());
		}
	}

	public String newJoke() {
		String[] joke = new String[8];
		joke[0] = "2b||!2b<br>";
		joke[1] = "Why can�t Java developer see anything? Because they can�t C#.<br>";
		joke[2] = "Was ist wei� und st�rt beim Essen? Eine Lawine.<br>";
		joke[3] = "Warum gehen Fliegen nicht in die Kirche? Sie sind Insekten.<br>";
		joke[4] = "Wie nennt man jemanden der DIN A4 Bl�tter scannt? � Skandinavier.<br>";
		joke[5] = "Was ist ein Kleines, schw�bisches, um Hilfe rufendes Schwein? Ein Notrufs�ule.<br>";
		joke[6] = "Steht ein Schwein vor einer Steckdose: �Na Kumpel, wer hat dich denn eingemauert?<br>";
		joke[7] = "Es gibt 10 Arten von Menschen. Die einen verstehen das Bin�rsystem, die anderen nicht.<br>";
		Random r = new Random();
		int randomNumber = r.nextInt(joke.length);
		return joke[randomNumber];
	}

	public void getCommentHaircolor(Person person, StringBuilder builder) {
		if (person.getHairColor().isNoHair()) {
			builder.append(person.getHairColor().getCommentHaircolor());
			builder.append("<br>");
		} else {
			builder.append("Deine Haarfarbe wirkt heute ");
			builder.append(person.getHairColor().getCommentHaircolor());
			builder.append(person.getHairColor().greyHair());
			builder.append("<br>");
		}

	}

	public void getCommentFacialMakeup(Person person, StringBuilder builder) {
		if (person.isLipmakeup() == false && person.isEyemakeup() == false) {
			if (person.getGender() == "female") {
				builder.append("Na, heute ungeschminkt?<br>");
			}
		} else if (person.isEyemakeup() == false && person.isLipmakeup() == true) {
			// pr�fen welche uhrzeit, augenmakeup hinweisen
			builder.append("Lippenstift ist aufgetragen.<br>");
		} else if (person.isEyemakeup() == true && person.isLipmakeup() == false) {
			builder.append("Die Augen sind sehr h�bsch geschminkt.<br>");
		} else if (person.isEyemakeup() && person.isLipmakeup()) {

			builder.append("Du siehst heute sehr h�bsch aus.<br>");
		}

	}

	private void additionalInformations(Weather wetter, Content content, Person person, StringBuilder builder) {

		if (wetter.getWeatherInfo().equals("Rain") && person.getGlases().equals("Sunglasses")) {
			builder.append("Du kannst doch nicht bei Regen eine Sonnenbrille tragen.<br>");
		}
		if (wetter.getWeatherInfo().equals("Rain")) {
			builder.append("Denke an den Regenschirm.<br>");
		}
		if (Double.parseDouble(wetter.getNowtemp()) <= 1) {
			builder.append("Es ist sehr kalt, ziehe lieber Handschuhe und eine M�tze an.<br>");
		}
		if ((wetter.getWeatherInfo().equals("Clear sky") || wetter.getWeatherInfo().equals("Clear"))
				&& person.isHeadware() == false) {
			builder.append("M�chtest du keinen Sonnenschutz tragen?<br>");
		}

		String weatherInfo = wetter.getWeatherInfo();

		if ((weatherInfo.equals("Clear sky") || weatherInfo.equals("Clear")) && person.getGlases().equals("NoGlasses")
				&& Double.parseDouble(wetter.getNowtemp()) >= 15.00) {
			builder.append("M�chtest du keine Sonnenbrille tragen?<br>");
		}

		if (person.getName().equals("Heidrun")
				&& person.getHairColor().getCommentHaircolor().toLowerCase().contains("schwarz")) {
			builder.append("Du solltest wiedereinmal zum F�rben gehen.<br>");
		}

		if (person.getBeard().getBeardStyle().equals("Stoppelbart") && !content.getEvents().isEmpty()
				&& person.getGender().equals("male")) {
			builder.append("Du solltest dich Rasieren du hast einen Termin!<br>");
		}

		if ((LocalDateTime.now().getHour() >= 20) && (person.getGender().toLowerCase().equals("female"))
				&& (!content.getEvents().isEmpty())
				&& (person.isLipmakeup() == false || person.isEyemakeup() == false)) {
			builder.append("Willst du dich nicht Schminken heute Abend?<br>");
		}
		commentAboutAge(builder, person);

	}

	private void commentAboutAge(StringBuilder output, Person person) {
		if (person.getBirthYear().getValue() > 0) {
			double age = LocalDateTime.now().getYear() - person.getBirthYear().getValue();
			int diff = (int) Math.abs(age - person.getAge());
			if (age > person.getAge()) {
				output.append(
						"Hattest du eine Verj�ngungskur? Du siehst " + diff + " Jahre j�nger aus als du bist.<br>");
			} else {
				if (age < person.getAge()) {
					output.append("Du brauchst Schlaf. Du siehst " + diff + " Jahre �lter aus als du bist.<br>");
				} else {
					output.append("");
				}
			}
		}

	}

	public void smilingPersonDetected(Content content, StringBuilder builder) {

		builder.append(content.getSmilesentence());

	}
}
