package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

import de.grimmig.smartmirror.domain.Person;

public class GreetingsPanel extends JPanel {
	private JLabel greetingsLabel;

	public GreetingsPanel() {
		setLayout(new BorderLayout());
		greetingsLabel = new JLabel();
		greetingsLabel.setForeground(Color.WHITE);
		greetingsLabel.setFont(new Font("Arial", Font.BOLD, 20));
		greetingsLabel.setVerticalAlignment(JLabel.CENTER);
		greetingsLabel.setHorizontalAlignment(JLabel.CENTER);
		
		setOpaque(false);
		add(greetingsLabel);
	}

	public void setContent(Person person) {
		greetingsLabel.setText(person.getWelcomeMessage());
	}
}
