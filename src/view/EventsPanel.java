package view;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;

public class EventsPanel extends JPanel {
	private JLabel eventsLabel;

	public EventsPanel() {
		eventsLabel = new JLabel();
		eventsLabel.setForeground(Color.WHITE);
		eventsLabel.setFont(new Font("Arial", Font.BOLD, 15));
		setOpaque(false);
		add(eventsLabel);
	}

	public void setEvents(List<Event> events) {

		String calendarOutput = "";
		if (events.isEmpty()) {
			calendarOutput = "<html><body>Heute stehen keine Termine an.";
		} else {
			calendarOutput = "<html><body>Kommende Termine";

			for (Event event : events) {
				DateTime start = event.getStart().getDateTime();
				if (start == null) {
					start = event.getStart().getDate();

				}
				calendarOutput += "<br>";
				calendarOutput += event.getSummary() + " ";
				// gibt den ort des Kalendereintrags aus
				if (event.getLocation() != null) {
					String location = event.getLocation();
					String[] arr = location.split(",", 2);
					calendarOutput += arr[0];
				}
				if (event.getStart().getDateTime() != null) {
					String startTime = event.getStart().getDateTime().toString();
					String[] arr = startTime.split("T|\\.", 3);
					calendarOutput += "Start: " + arr[1] + " ";
				}
				if (event.getEnd().getDateTime() != null) {
					String endTime = event.getEnd().getDateTime().toString();
					String[] arr = endTime.split("T|\\.", 3);
					calendarOutput += "Ende: " + arr[1];
				}
			}

		}
		calendarOutput += "</body></html>";
		eventsLabel.setText(calendarOutput);
	}
	
	
}
