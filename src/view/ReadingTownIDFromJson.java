package view;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

//import org.json.JSONArray;
//import org.json.JSONObject;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReadingTownIDFromJson {
	private String townID;

	public String returnTownID(String town, String country) {

		JSONParser parser = new JSONParser();
		JSONArray jsonArr;
		try {
			jsonArr = (JSONArray) parser.parse(new FileReader("..\\FirstOpenCV\\cityList.json"));
			for (int i = 0; i < jsonArr.size(); i++) {
				JSONObject tmpObject = (JSONObject) jsonArr.get(i);
				if (tmpObject.get("name").toString().toLowerCase().equals(town)) {
					if (!country.isEmpty()) {
						switch (country.toLowerCase()) {
						case " deutschland":
							tmpObject.get("country").equals("DE");
							townID = tmpObject.get("id").toString();
							return townID;

						default:
							break;
						}
					} else {
						townID = tmpObject.get("id").toString();
						return townID;
					}

				}
			}
			return null;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public String getTownID() {
		return townID;
	}

	public void setTownID(String townID) {
		this.townID = townID;
	}

}
