package view;

import java.time.LocalDateTime;
import java.util.List;

import com.google.api.services.calendar.model.Event;

public class Content {
	private boolean grinning = false;
	private String smilesentence = "";

	private boolean showRemindings = false;
	private List<Event> events = null;

	public String weatherAfterSettingCalendar() {
		String townID = "";
		if (!events.isEmpty()) {
			String startTime = events.get(0).getStart().getDateTime().toString();
			String[] arr = startTime.split("T|\\:", 3);
			int startzeit = Integer.parseInt(arr[1]);
			if (LocalDateTime.now().getHour() == startzeit || LocalDateTime.now().getHour() == startzeit - 1) {
				String locationOfEvent = events.get(0).getLocation().toLowerCase();
				String[] arrLocation = locationOfEvent.split("\\,", 2);
				String townLocation = arrLocation[0];
				String country = arrLocation[1];
;//				switch (townLocation) {
//				case "karlsruhe":
//					townID = "3214104";
//					break;
//				case "oberkirch":
//					townID = "2860080";
//					break;
//				case "kehl":
//					townID = "2891951";
//					break;
//				case "offenburg":
//					townID = "2857798";
//					break;
//				default:
//					break;
//				}
				ReadingTownIDFromJson getTownID = new ReadingTownIDFromJson();
				townID=getTownID.returnTownID(townLocation,country );

			}
		}
		return townID;

	}

	public String getSmilesentence() {
		return smilesentence;
	}

	public void setSmilesentence() {
		if (grinning) {
			this.smilesentence = "Wie sch�n es ist wenn man angel�chelt wird :)<br>";
		} else {
			this.smilesentence = "Jetzt lach doch mal :D !<br>";
		}
	}

	public boolean getGrinning() {
		return grinning;
	}

	public void setGrinning(boolean grinsen) {
		this.grinning = grinsen;
		setSmilesentence();
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public boolean isShowRemindings() {
		return showRemindings;
	}

	public void setShowRemindings(boolean erinnerungenAnzeigen) {
		this.showRemindings = erinnerungenAnzeigen;
	}

}
