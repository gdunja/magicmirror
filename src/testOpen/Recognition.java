package testOpen;

import static org.bytedeco.javacpp.opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

import java.io.File;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;

import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_face.EigenFaceRecognizer;
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;
import org.bytedeco.javacpp.opencv_face.FisherFaceRecognizer;
import org.bytedeco.javacpp.opencv_face.LBPHFaceRecognizer;

import de.grimmig.smartmirror.domain.Person;
import de.grimmig.smartmirror.weather.Weather;
import de.grimmig.smartmirror.weather.WeatherService;
import viedeostreaming.Facedetect;
import view.Content;
import view.GoogleCalendar;
import view.Main;

import view.Window;

public class Recognition {
	// private Thread runner;
	// private LocalDateTime twoMinutesLater = LocalDateTime.now().plusMinutes(2);
	private Window window;

	// public static void main(String[] args) {
	public void start(Window window) {
		try {
			// Window window = new Window();
			// System.out.println("l�dt window");
			this.window = window;
			Content content = new Content();
			System.out.println("l�dt inhalt");
			Main azure = new Main();
			System.out.println("l�dt main");
			WeatherService weatherService = new WeatherService("548b4a6d87c2bc48f530857623b469c2");
			System.out.println("l�dt wetter");
			GoogleCalendar google = new GoogleCalendar();
			System.out.println("l�dt google calendar");
			// hier stoppt das Programm bei der Ausf�hrung!!!!
			String testImageFileString = "../FirstOpenCV/NeuerOrdner/out.jpg";
			String wholeImageFileString = "../FirstOpenCV/NeuerOrdner/wholeOut.jpg";
			Recognition recogn = new Recognition();
			// Person person =recogn.whichPerson(getEigenfacesRecognition(testImageFileString));
			Person person = recogn.whichPerson(getFisherFacesRecognition(testImageFileString));
			//Person person = recogn.whichPerson(getLBPHRecognition(testImageFileString));

			google.getGoogle(content);
			SmileDetection smile = new SmileDetection();
			Weather weather;
			System.out.println("contentweatherempty"+content.weatherAfterSettingCalendar());
			if (content.weatherAfterSettingCalendar()==null||content.weatherAfterSettingCalendar().isEmpty()||content.weatherAfterSettingCalendar().equals("")) {
				weather = weatherService.getWeather(person.getDefaultTown());
				} else {
					weather = weatherService.getWeather(content.weatherAfterSettingCalendar());
				}

			smile.detect(testImageFileString, person, content);
			azure.getAzureInformation(wholeImageFileString, content, person);
			person.begruessungsSatzFestlegen();
			window.showWindow(content, weather, person);
			// this.runner = new Thread(this);
			// this.runner.start();

		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	// public void run() {
	// while (true) {
	//
	//// if (twoMinutesLater.isAfter(LocalDateTime.now())) {
	//// window.closeWindow();
	//// Facedetect facedetect = new Facedetect();
	//// facedetect.startFaceDetection(window);
	//// }
	// try {
	// Thread.sleep(1000);
	// } catch (InterruptedException ex) {
	// // TODO: handle exception
	// }
	// }
	// }

	public Person whichPerson(int predictedLabel) {

		switch (predictedLabel) {
		case 1:
			return new Person("Dunja", "3214104", Year.of(1992), true);
		case 2:
			return new Person("Heidrun", "2891951", Year.of(1964), false);
		case 3:
			return new Person("Herr Eisenbiegler", "3214104", Year.of(0), false);
		case 4:
			return new Person("Flo", "2860080", Year.of(1992), true);
		case 5:
			return new Person("Linda", "3220838", Year.of(1993), true);
		default:
			return new Person("unbekannt", "3214104", Year.of(0), true);
		}

	}

	public int getEigenfacesRecognition(String testImageFileString) {
		FaceRecognizer faceRecognizer = EigenFaceRecognizer.create();
		System.out.println("l�dt recognizer");
		faceRecognizer.read("eigenfaces_at.yml");
		System.out.println("lie�t eigenfaces yml");

		// fehlermeldung einbauen
		File testImageFile = new File(testImageFileString);
		Mat testImage = imread(testImageFile.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
		IntPointer label = new IntPointer(1);
		DoublePointer confidence = new DoublePointer(1);
		faceRecognizer.predict(testImage, label, confidence);
		System.out.println("l�dt personenerkennung");
		int predictedLabel = label.get(0);
		return predictedLabel;
	}

	public int getLBPHRecognition(String testImageFileString) {
		FaceRecognizer faceRecognizer = LBPHFaceRecognizer.create();
		System.out.println("l�dt recognizer");
		faceRecognizer.read("eigenfaces_lbph.yml");
		System.out.println("lie�t lbph yml");

		// fehlermeldung einbauen
		File testImageFile = new File(testImageFileString);
		Mat testImage = imread(testImageFile.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
		IntPointer label = new IntPointer(1);
		DoublePointer confidence = new DoublePointer(1);
		faceRecognizer.predict(testImage, label, confidence);
		System.out.println("l�dt personenerkennung");
		int predictedLabel = label.get(0);
		return predictedLabel;
	}

	public int getFisherFacesRecognition(String testImageFileString) {
		FaceRecognizer faceRecognizer = FisherFaceRecognizer.create();
		System.out.println("l�dt recognizer");
		faceRecognizer.read("eigenfaces_fisher.yml");
		System.out.println("lie�t fisherfaces yml");

		// fehlermeldung einbauen
		File testImageFile = new File(testImageFileString);
		Mat testImage = imread(testImageFile.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
		IntPointer label = new IntPointer(1);
		DoublePointer confidence = new DoublePointer(1);
		faceRecognizer.predict(testImage, label, confidence);
		System.out.println("l�dt personenerkennung");
		int predictedLabel = label.get(0);
		return predictedLabel;
	}

}
