package testOpen;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

public class Main {

	public static void main(String[] args) {
		// testen der Main Klasse
		Main main = new Main();
		main.makerun();

	}

	// auswahl der Bilder damit nicht jedes einzelne ausgew�hlt werden muss
	public void makerun() {
		String file;
		for (int i = 70; i <= 71; i++) {
			file = "..\\test (" + i + ").jpg";
			detect(file, i);
		}
	}

	public void detect(String file, int number) {
		// laden von OpenCV
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		// einlesen des Bildes und umwandeln in Vektor matrix
		Mat src = Imgcodecs.imread(file);
		// auswahl der "Ansicht
		String xmlFile = "C:\\Users\\Dunja\\workspaceub\\FirstOpenCV\\src\\testOpen\\haarcascade_frontalface_alt.xml";
		CascadeClassifier classifier = new CascadeClassifier(xmlFile);
		// erzeugen eines Rechtecks um das Gesicht
		MatOfRect faceDetections = new MatOfRect();
		// pr�fen ob gesicht bzw. bild vorhanden
		if (src.empty()) {
			System.out.println("imageless");
		}
		if (classifier.empty()) {
			System.out.println("faceless");
		}
		// herausfiltern der Gesichter
		classifier.detectMultiScale(src, faceDetections);
		System.out.println(String.format("Detected %s faces ", faceDetections.toArray().length));
		// Gr��e des GEsichts festlegen
		Rect rectCrop = null;
		for (Rect rect : faceDetections.toArray()) {
			Imgproc.rectangle(src, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
					new Scalar(0, 0, 255), 3);
			rectCrop = new Rect(rect.x, rect.y, rect.width, rect.height);
		}
		// rechteck und bild mit rechteck speichern
		Mat image_roi = new Mat(src, rectCrop);
		Mat img = new Mat();
		Imgproc.resize(image_roi, img, new Size(100, 100));
		Imgcodecs.imwrite("C:\\Users\\Dunja\\workspaceub\\FirstOpenCV\\facedetect_face" + number + ".jpg", img);
		Imgcodecs.imwrite("C:\\Users\\Dunja\\workspaceub\\FirstOpenCV\\facedetect_output" + number + ".jpg", src);
		System.out.println("Image Processed");
	}

}
