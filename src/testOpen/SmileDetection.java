package testOpen;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.objdetect.CascadeClassifier;

import de.grimmig.smartmirror.domain.Person;
import view.Content;

public class SmileDetection {
	
	public void detect(String file, Person person, Content inhalt) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat src = Imgcodecs.imread(file);

		String xmlFile = "..\\FirstOpenCV\\src\\testOpen\\haarcascade_smile.xml";
		CascadeClassifier classifier = new CascadeClassifier(xmlFile);

		MatOfRect faceDetections = new MatOfRect();
		if (src.empty()) {
			System.out.println("imageless");
		}
		if (classifier.empty()) {
			System.out.println("smileless");
		}

		classifier.detectMultiScale(src, faceDetections);
		// System.out.println(String.format("Detected %s smiles ",
		// faceDetections.toArray().length));

		if (faceDetections.toArray().length == 1) {
			inhalt.setGrinning(true);
		} else {
			if (faceDetections.toArray().length > 1) {
				inhalt.setGrinning(false);
			} else {
				inhalt.setGrinning(false);
			}
		}

	}

}
