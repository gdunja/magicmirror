package testOpen;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.IntBuffer;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;

import static org.bytedeco.javacpp.opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;

import org.bytedeco.javacpp.opencv_face.FisherFaceRecognizer;
import org.bytedeco.javacpp.opencv_face.EigenFaceRecognizer;
import org.bytedeco.javacpp.opencv_face.LBPHFaceRecognizer;
import org.opencv.core.Core;

public class FaceTrainer {

	public static void main(String[] args) {
		//l�dt die Bibliothek
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		//legt das Trainingsverzeichnis fest
		String trainingDir = "C:\\Users\\Dunja\\workspaceub\\DetectedFaces";

		File root = new File(trainingDir);
		//filtert nach Bildern
		FilenameFilter imgFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				name = name.toLowerCase();
				return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
			}
		};
		//�bertr�gt es in eine Liste
		File[] imageFiles = root.listFiles(imgFilter);
		
		MatVector images = new MatVector(imageFiles.length);

		Mat labels = new Mat(imageFiles.length, 1, CV_32SC1);
		IntBuffer labelsBuf = labels.createBuffer();

		int counter = 0;

		for (File image : imageFiles) {
			Mat img = imread(image.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
			int label = Integer.parseInt(image.getName().split("\\-")[0]);
			images.put(counter, img);
			labelsBuf.put(counter, label);

			counter++;
		}

		FaceRecognizer faceRecognizer = FisherFaceRecognizer.create();
		// FaceRecognizer faceRecognizer = EigenFaceRecognizer.create();
		// FaceRecognizer faceRecognizer = LBPHFaceRecognizer.create();

		faceRecognizer.train(images, labels);
		faceRecognizer.save("eigenfaces_fisher.yml");

		
	}

}
