package de.grimmig.smartmirror.weather;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.swing.ImageIcon;

public class Weather {
	private String name;
	private String nowtemp;
	private String minTemp;
	private String maxTemp;
	private String iconName;
	private String weatherInfo;

	public Weather(String name, String nowtemp, String minTemp, String maxTemp, String iconName, String weatherInfo) {
		super();
		this.name = name;
		this.nowtemp = nowtemp;
		this.minTemp = minTemp;
		this.maxTemp = maxTemp;
		this.iconName = iconName;
		this.weatherInfo = weatherInfo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNowtemp() {
		return nowtemp;
	}

	public void setNowtemp(String nowtemp) {
		this.nowtemp = nowtemp;
	}

	public String getMinTemp() {
		return minTemp;
	}

	public void setMinTemp(String minTemp) {
		this.minTemp = minTemp;
	}

	public String getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(String maxTemp) {
		this.maxTemp = maxTemp;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconname) {
		this.iconName = iconname;
	}

	public String getWeatherInfo() {
		return weatherInfo;
	}

	public void setWeatherInfo(String weatherInfo) {
		this.weatherInfo = weatherInfo;
	}

}
