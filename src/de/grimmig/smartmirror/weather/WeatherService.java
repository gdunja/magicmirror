package de.grimmig.smartmirror.weather;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class WeatherService {

	private String apiKey;
	 
	public WeatherService(String apiKey) {
		this.apiKey= apiKey;
	}
	
	public Weather getWeather(String town) throws MalformedURLException, IOException {

		String urlname = "https://api.openweathermap.org/data/2.5/weather?id=" + town + "&appid=" + apiKey
				+ "&units=metric&lang=de";
		URL url = new URL(urlname);

		try (InputStream is = url.openStream(); JsonReader rdr = Json.createReader(is)) {

			JsonObject obj = rdr.readObject();
			String name = obj.get("name").toString();
			JsonObject main = obj.getJsonObject("main");
			String nowTemp = main.get("temp").toString();
			JsonArray weatherJson = obj.getJsonArray("weather");
			JsonObject id = weatherJson.getJsonObject(0);
			String minTemp = main.get("temp_min").toString();
			String maxTemp = main.get("temp_max").toString();
			String weatherInfo = id.get("main").toString().replace("\"", "");
			String iconName = id.get("icon").toString().replace("\"", "");
			return new Weather(name, nowTemp, minTemp, maxTemp, iconName, weatherInfo);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw e;
		}
	}
}
