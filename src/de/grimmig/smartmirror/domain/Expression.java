package de.grimmig.smartmirror.domain;

public class Expression {
	private double contempt = 0;
	private double surprise = 0;
	private double happiness = 0;
	private double neutral = 0;
	private double sadness = 0;
	private double disgust = 0;
	private double anger = 0;
	private double fear = 0;
	
	public String getCommentMood() {
		String	mood = "";
			if (contempt > 0.6) {
				mood += "ver�chtlich ";
			}
			if (surprise > 0.6) {
				mood += "�berrascht ";
			}
			if (happiness > 0.6) {
				mood += "gl�cklich ";
			}
			if (neutral > 0.8) {
				mood += "neutral ";
			}
			if (sadness > 0.6) {
				mood += "traurig ";
			}
			if (disgust > 0.6) {
				mood += "angewiedert ";
			}
			if (anger > 0.6) {
				mood += "w�tend ";
			}
			if (fear > 0.6) {
				mood += "�ngstlich ";
			}
			if(mood.isEmpty()) {
				mood+= "neutral ";
			}
			mood += "";

			
			return mood;
		}
	
	public double getContempt() {
		return contempt;
	}
	public void setContempt(double contempt) {
		this.contempt = contempt;
	}
	public double getSurprise() {
		return surprise;
	}
	public void setSurprise(double surprise) {
		this.surprise = surprise;
	}
	public double getHappiness() {
		return happiness;
	}
	public void setHappiness(double happiness) {
		this.happiness = happiness;
	}
	public double getNeutral() {
		return neutral;
	}
	public void setNeutral(double neutral) {
		this.neutral = neutral;
	}
	public double getSadness() {
		return sadness;
	}
	public void setSadness(double sadness) {
		this.sadness = sadness;
	}
	public double getDisgust() {
		return disgust;
	}
	public void setDisgust(double disgust) {
		this.disgust = disgust;
	}
	public double getAnger() {
		return anger;
	}
	public void setAnger(double anger) {
		this.anger = anger;
	}
	public double getFear() {
		return fear;
	}
	public void setFear(double fear) {
		this.fear = fear;
	}
	
	
}
