package de.grimmig.smartmirror.domain;

public class HairColor {
	private double brown = 0;
	private double black = 0;
	private double other = 0;
	private double blond = 0;
	private double red = 0;
	private double gray = 0;
	private boolean noHair =false;

	public String getCommentHaircolor() {
		String hairColorMessage = "";
		if (getBrown() > 0.6) {
			hairColorMessage += "braun ";
		}
		if (getRed() > 0.6) {
			hairColorMessage += "rot ";
		}
		if (getBlond() > 0.6) {
			hairColorMessage += "blond ";
		}
		if (getBlack() > 0.6) {
			hairColorMessage += "schwarz ";
		}

		if (getGray() > 0.6) {
			hairColorMessage += "grau";
		}
		if (getOther() > 0.7) {
			hairColorMessage += "etwas undefinierbar ";
		}
		if(noHair) {
			hairColorMessage+="ein sch�nes Gesicht braucht platz ;)";
		}
		if(hairColorMessage.isEmpty()) {
			hairColorMessage += "nicht erkennbar ";
		}
		return hairColorMessage + ".<br>";
	}

	public String greyHair() {
		String grey = "";
		if (getGray() > 0.6) {
			grey = "Was sehe ich da? Ein graues Haar :) <br>";
		}
		return grey;
	}

	public double getBrown() {
		return brown;
	}

	public void setBrown(double brown) {
		this.brown = brown;
	}

	public double getBlack() {
		return black;
	}

	public void setBlack(double black) {
		this.black = black;
	}

	public double getOther() {
		return other;
	}

	public void setOther(double other) {
		this.other = other;
	}

	public double getBlond() {
		return blond;
	}

	public void setBlond(double blond) {
		this.blond = blond;
	}

	public double getRed() {
		return red;
	}

	public void setRed(double red) {
		this.red = red;
	}

	public double getGray() {
		return gray;
	}

	public void setGray(double gray) {
		this.gray = gray;
	}

	public boolean isNoHair() {
		return noHair;
	}

	public void setNoHair(boolean noHair) {
		this.noHair = noHair;
	}

}
