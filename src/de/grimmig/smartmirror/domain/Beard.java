package de.grimmig.smartmirror.domain;

public class Beard {
	private double sideburns = 0;
	private double beard = 0;
	private double moustache = 0;

	public String getBeardStyle() {
		
				if((beard> 0 && beard <= 0.4)
						|| (moustache > 0 && moustache <= 0.4)
						|| (sideburns > 0 && sideburns <= 0.4)) {
					return "Stoppelbart";
				}
				if(beard>0.4) {
					return "Bart";
				}
				if(moustache>0.4) {
					return "Schnautzer";
				}
				if(beard>0.4&&moustache>0.4&&sideburns>0.4) {
					return "Vollbart";
				}
				else {
					return "kein Bart";
				}
	}
	
	public double getSideburns() {
		return sideburns;
	}

	public void setSideburns(double sideburns) {
		this.sideburns = sideburns;
	}

	public double getBeard() {
		return beard;
	}

	public void setBeard(double beard) {
		this.beard = beard;
	}

	public double getMoustache() {
		return moustache;
	}

	public void setMoustache(double moustache) {
		this.moustache = moustache;
	}

}
