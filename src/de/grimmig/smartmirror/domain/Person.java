package de.grimmig.smartmirror.domain;

import java.time.Year;

public class Person {

	private HairColor hairColor;
	private Beard beard;

	private Expression expression;

	private Year birthYear;
	private String defaultTown;
	private String name;
	private boolean wantToSeeRemindings;
	private String gender;
	private String glases;
	private boolean eyemakeup;
	private boolean lipmakeup;
	private String welcomeMessage;
	private boolean headware;
	private double age ;

	public Person(String name, String defaultTown, Year birthYear, boolean wantToSeeRemindings) {
		hairColor = new HairColor();
		expression = new Expression();
		beard = new Beard();
		glases = "";
		age = 0;
		eyemakeup = false;
		lipmakeup = false;
		headware = false;
		this.name = name;
		this.defaultTown = defaultTown;
		this.birthYear = birthYear;
		this.wantToSeeRemindings = wantToSeeRemindings;

	}

	public void begruessungsSatzFestlegen() {
		if (name.equals("unbekannt")) {
			if (gender.equals("male")) {
				welcomeMessage = "Guten Tag der Herr";
			} else {
				welcomeMessage = "Guten Tag die Dame";
			}
		} else {
			welcomeMessage = "Hallo " + name;
		}
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Year getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(Year birthYear) {
		this.birthYear = birthYear;
	}

	public String getDefaultTown() {
		return defaultTown;
	}

	public void setDefaultTown(String defaultTown) {
		this.defaultTown = defaultTown;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isWantToSeeRemindings() {
		return wantToSeeRemindings;
	}

	public void setWantToSeeRemindings(boolean wantToSeeRemindings) {
		this.wantToSeeRemindings = wantToSeeRemindings;
	}

	public HairColor getHairColor() {
		return hairColor;
	}

	public void setHairColor(HairColor hairColor) {
		this.hairColor = hairColor;
	}

	public Expression getExpression() {
		return expression;
	}

	public void setExpression(Expression expression) {
		this.expression = expression;
	}

	public String getGlases() {
		return glases;
	}

	public void setGlases(String glases) {
		this.glases = glases;
	}

	public boolean isEyemakeup() {
		return eyemakeup;
	}

	public void setEyemakeup(boolean eyemakeup) {
		this.eyemakeup = eyemakeup;
	}

	public boolean isLipmakeup() {
		return lipmakeup;
	}

	public void setLipmakeup(boolean lipmakeup) {
		this.lipmakeup = lipmakeup;
	}

	public Beard getBeard() {
		return beard;
	}

	public void setBeard(Beard beard) {
		this.beard = beard;
	}

	public String getWelcomeMessage() {
		return welcomeMessage;
	}

	public void setWelcomeMessage(String welcomeMessage) {
		this.welcomeMessage = welcomeMessage;
	}

	public boolean isHeadware() {
		return headware;
	}

	public void setHeadware(boolean headware) {
		this.headware = headware;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

}
